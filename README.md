

## Quick summary ##

* * *
## Version ##
Interview

* * *
## Setup ##
 1. Checkout code (git clone)
 2. Go into the project folder
 3. Execute command `npm install` (that will install all module dependencies)
 4. Run : `react-native run-ios` (Android version is not implemented)

 NOTE: Maybe it needs to change the team in the signing section into Xcode.

## Features implemented ##
* Focus the work on iOS (but will work the same on Android)
* Use react-navigation for navigating inside the app
* Load RSS Feed
* Main screen - Display list of feeds
* Detail screen - Display content of a feed
* ES6
* Added activity indicator in order to refresh the list of feed
* Persistant data: Started this task but no finish. For that I decided to use redux, redux-persist. I implemented redux but did not finish with persist and autohydrate()
For redux: see folder "Actions", "Reducers" and "Services" + store.js.

I would like to implement a model in order to be able to load different RSS Feeds but did not jump too much in
this area because it needs more time.
But the idea is to have redux in order to save different feeds and load them locally when there is not connection.
It can be good to implement an indicator in the model in order to know which feeds are or not read by the user.
