/**
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { ActionCreators } from '../../actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

// SERVICES
import  RSSService from '../../services/RSSService';

// STYLES
import GlobalColors from '../../global/globalColors';
import styles from './styles';

export class Main extends Component<{}> {

  constructor(props) {
    super(props);

    this.initialState = {
      isLoading: this.props.RSSFeeds.isLoading,
    };

    this.state = this.initialState;
  }

  componentDidMount = () => {
    this.getRSSFeedsEngineering();
  }

  getRSSFeedsEngineering = ()  => {
    this.props.getRSSFeedsEngineering();
  }

  renderItem = (row) => {
    let item = row.item;

    if(!item) {
      return(<View></View>);
    }
    return (
      <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.navigate('FeedDetail', {url:item.link})}>
        <View key={item.guid}>
          <View style={styles.row}>
            <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">{item.title}</Text>
            <Text style={styles.date}>{moment(item.pubDate).format("MMM Do YY")}</Text>
            <Text style={styles.description} numberOfLines={2} ellipsizeMode="tail">{item.description}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render = () => {

    if(this.props.RSSFeeds.isLoading == null || this.props.RSSFeeds.isLoading ) {
      return(
        <View style={styles.container}>
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <FlatList
            data={this.props.RSSFeeds.channels.channel.item}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => {return(item.guid)}}
            refreshing={this.props.RSSFeeds.isLoading}
            onRefresh={this.getRSSFeedsEngineering}
          />
        </View>
      );
    }
  }

}

// Meta
function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
    RSSFeeds: state.RSSFeeds,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Main, 'Main');
