import { StyleSheet } from 'react-native';

import GlobalColors from '../../global/globalColors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
  row: {
    paddingTop:5,
    paddingBottom:5,
    marginLeft:10,
    marginRight:10,
    borderBottomColor: '#E0E0E0',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  title: {
    fontSize: 13,
    fontWeight: 'bold'
  },
  date: {
    fontSize: 13,
    color: "#909497",
  },
  description: {
    fontSize: 9,
  },
});
