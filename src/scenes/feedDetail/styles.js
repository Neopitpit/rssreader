import { StyleSheet } from 'react-native';

import GlobalColors from '../../global/globalColors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
  },
});
