/**
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  WebView
} from 'react-native';
import { ActionCreators } from '../../actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// STYLES
import GlobalColors from '../../global/globalColors';
import styles from './styles';

export class FeedDetail extends Component<{}> {

  constructor(props) {
    super(props);
  }

  render = () => {
    return (
      <WebView
        source={{uri: this.props.navigation.state.params.url}}
      />
    );
  }
}

// Meta
function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FeedDetail, 'FeedDetail');
