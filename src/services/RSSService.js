/**
 * RSSService.js
 * @flow
 */

import API from '../helpers/api';
import * as GlobalParameters from '../global/globalParameters';

class RSSService {

  static getRSSFeedsEngineering = () => {
    const url = `${GlobalParameters.PROTOCOL}${GlobalParameters.API}${GlobalParameters.RSSFeedEndPoints.engineering}`;
    return API.get(url).catch((e) => {
      throw  e
    });
  }

}

export default RSSService;
