/**
 * scenes.js
 * @flow
 */

import React, { Component } from 'react';

import {
  Platform,
  StatusBar
} from 'react-native';

import { ActionCreators } from './actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createRootNavigator } from './global/router';

class InterviewApp extends Component {
  // Constructor and props
  constructor(props) {
    super(props);
  }

  // States
  componentWillMount() {
  }

  componentDidMount() {
  }

  getCurrentRouteName(navigationState) {
    if (!navigationState) {
      return null;
    }

    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    if (route.routes) {
      return this.getCurrentRouteName(route);
    }

    return route.routeName;
  }

  // Rendering
  render() {
    let Layout = createRootNavigator();
    return (
      <Layout onNavigationStateChange={
        (prevState, currentState) => {
          let currentStateName = this.getCurrentRouteName(currentState)
        }
      }/>
    );
  }
}

// Meta
function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(InterviewApp);
