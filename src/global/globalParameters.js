/**
 * globalParameters.js
 * @flow
 */

export const PROTOCOL = 'https://';
export const API = 'dl.dropboxusercontent.com/s'; // I considered this part as the root of the API

export const RSSFeedEndPoints = {
  engineering: '/cpl8uh9mloelmz2/invision-blog.json?dl=0',
};
