/**
 * globalColors.js
 * @flow
 */

import { Platform, Dimensions, PixelRatio } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const platform = Platform.OS;
const platformStyle = undefined;

export default {
  platformStyle,
  platform,

  // Color
  brandPrimary: '#000',
  brandSecondary: '#FFF',

  // Device screen size
  deviceWidth,
  deviceHeight,

};
