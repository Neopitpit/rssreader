/**
 * router.js
 * @flow
 */

import React from 'react';
import { TabNavigator, StackNavigator } from 'react-navigation';

import FeedMainScene from '../scenes/feedMain';
import FeedDetailScene from '../scenes/feedDetail';

export const createRootNavigator = () => {
  return StackNavigator({
    FeedMain: {
      screen: FeedMainScene,
      navigationOptions: {
        title: 'Dashboard',
      }
    },
    FeedDetail: {
      screen: FeedDetailScene,
      navigationOptions: {
        title: 'Details',
      }
    },
  });
};
