/**
 * userReducer.js
 * @flow
 */

import createReducer from '../helpers/createReducers';
import * as actionTypes from '../actions/actionTypes';

export type State = {
  error: ?string,
  channels: ?object,
  isLoading: ?bool,
}

export const initialState = {
  channels: [],
  isLoading: true,
};

export const RSSFeeds = createReducer({}, {
  [actionTypes.BLOG_GET_RSSFEED_SUCCEED](state: State, action) {
    return {
      ...state,
      channels:action.RSSFeeds.rss,
      error: null,
      isLoading:false,
    };
  },
  [actionTypes.BLOG_GET_RSSFEED_PENDING](state: State, action) {
    return {
      ...state,
      isLoading:true,
    };
  },
  [actionTypes.TBLOG_GET_RSSFEED_REJECTED](state: State, action) {
    return {
      ...state,
      error: action.error,
    };
  },
  [actionTypes.NONE](state: State = initialState, action) {
    return state;
  }
});
