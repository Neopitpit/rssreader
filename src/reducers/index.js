/**
 * index.js
 * @flow
 */

import { combineReducers } from 'redux';
import * as RSSFeedsReducer from './RSSFeedsReducer';

export default combineReducers(Object.assign(
    RSSFeedsReducer,
));
