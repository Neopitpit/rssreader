/**
 * api.js
 * @flow
 */

import {
  Alert
} from 'react-native';

class Api {
  static headers() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
  }

  static get(route) {
    return this.xhr(route, null, 'GET');
  }

  static put(route, params) {
    return this.xhr(route, params, 'PUT');
  }

  static post(route, params) {
    return this.xhr(route, params, 'POST');
  }

  static delete(route, params) {
    return this.xhr(route, params, 'DELETE');
  }

  static xhr(route, params, verb) {

    const options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null);
    options.headers = Api.headers();

    return new Promise((resolve, reject) => {
      let _isError = false;

      fetch(route, options).then((response) => {
        if (response.status === 200) { // No body in response
          return resolve(response.json());

        } else if(response.status === 401) { // Forbidden
          Alert.alert(I18n.t('error_expire'));

        } else if(response.status > 300) {
          _isError = true;
        }

        return response.json()

      }).then((data) => {
        return _isError ? reject(data) : resolve(data);

      }, (rejectData) => {
        return reject(rejectData);

      }).catch(e => {
        console.log(e)
        if (e.message == 'Network request failed') {

        } else {
          throw e;
        }
      });
    });
  }

}

export default Api;
