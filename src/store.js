/**
 * store.js
 * @flow
 */

import { createStore, applyMiddleware, compose } from 'redux';

import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import reducer from './reducers';
import { autoRehydrate, persistStore } from 'redux-persist'

// Create a logger to track all redux action, call but show that only in DEV MODE
const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__ });

// The store
const store = configureStore({});

// Configure the store
function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware,
    ),
    autoRehydrate()
  );

  return createStore(reducer, initialState, enhancer);
}


export default store;
