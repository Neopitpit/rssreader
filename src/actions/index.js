/**
 * index.js
 * @flow
 */

import * as RSSFeedsActions from './RSSFeeds';

export const ActionCreators = Object.assign({},
  RSSFeedsActions,
);
