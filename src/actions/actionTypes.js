/**
 * actionTypes.js
 * @flow
 */

export const NONE = 'NONE';

export const RESET_STORE = 'RESET_STORE';

// RSSFEED
export const BLOG_GET_RSSFEED_PENDING = 'blog/GET_RSSFEED_PENDING';
export const BLOG_GET_RSSFEED_SUCCEED = 'blog/GET_RSSFEED_SUCCEED';
export const BLOG_GET_RSSFEED_REJECTED = 'blog/GET_RSSFEED_REJECTED';
