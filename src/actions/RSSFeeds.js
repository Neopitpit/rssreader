/**
 * user.js
 * @flow
 */

import * as ActionTypes from './actionTypes';
import RSSService from '../services/RSSService';

//////////////////////////////////////////////////////////////////////////////
// REGISTRATION //////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
export function setRSSFeedAction({ RSSFeeds }) {
  return {
    type: ActionTypes.BLOG_GET_RSSFEED_SUCCEED,
    RSSFeeds
  };
}

export function setRSSFeedPending() {
  return {
    type: ActionTypes.BLOG_GET_RSSFEED_PENDING,
  };
}

export function setRSSFeedRejected({ error }) {
  return {
    type: ActionTypes.BLOG_GET_RSSFEED_REJECTED,
    error
  };
}

export function getRSSFeedsEngineering() {
  return (dispatch, getState) => {
    dispatch(setRSSFeedPending());
    return RSSService.getRSSFeedsEngineering()
      .then((resp) => {
        dispatch(setRSSFeedAction({ RSSFeeds: resp }));
      }).catch((ex) => {
        dispatch(setRSSFeedRejected({error: ex}));
        throw ex;
      })
  }
}
